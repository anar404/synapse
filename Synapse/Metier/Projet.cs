﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
    [Serializable]
    public class Projet
    {
        
        private string _nomProjet;
        private DateTime _dateDebut;
        private DateTime _dateFin;
        private decimal _prixFactureMO;
        private List<Mission> _listeMissions;

        //public decimal MargeBruteCourante()
        //{
        //    int marge = _prixFactureMO-
        //}

        private decimal CumulCoutMo()
        {
            decimal cumulCout = 0;
            foreach (Mission missionCourante in _listeMissions)
            {
                decimal tauxHoraire = missionCourante.Executant.TauxHoraire;
                decimal nbHeures = missionCourante.NbHeuresEffectues();
                cumulCout += tauxHoraire * nbHeures;
            }

            return cumulCout;
        }
    }
}
