﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
    [Serializable]
    public class Mission
    {
        private string _nomMission;
        private string _descriptionMission;
        private int _nbHeuresPrevues;
        private Dictionary<DateTime,int> _releveHoraire;
        private Intervenant _executant;

        public Intervenant Executant
        {
            get
            {
                return _executant;
            }
        }

        public Dictionary<DateTime, int> ReleveHoraire
        {
            get
            {
                return _releveHoraire;
            }

        }
        
        public void AjouteReleve(DateTime date, int nbHeures)
        {
            ReleveHoraire.Add(date, nbHeures);
        }

        public int NbHeuresEffectues()
        {
            int nbHeures = 0;
            foreach (KeyValuePair<DateTime,int> jourCourant in ReleveHoraire)
            {
                nbHeures += jourCourant.Value;
            }

            return nbHeures;
        }
    }  
}
